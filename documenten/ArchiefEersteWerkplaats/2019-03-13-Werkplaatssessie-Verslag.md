# Verslag Werkplaatssessie 13 Maart 2019
Verslag: Wilko Quak



Lennert Luik (product owner van het LVBB) was op bezoek. Hij heeft een demo
gegeven van het  previewportaal. In het previewportaal kan je een soort
proefpublicatie van je besluit doen. Wanneer deze proefpublictie niet lukt komt
het portaal met een zinnige foutmelding. De tool is nog niet publiekelijk
beschikbaar, maar als het zover is zat dit gemeld worden naar de deelnemers van
de werkplaats.

Daarnaast hebben een rondje gemaakt met globale bevindingen:

- We hebben nu wel voorbeeldbestanden, maar wie zegt dat ze goed zijn; er zijn
  nog veel vragen. Ik ga proberen zoveel mogelijk vragen te beantwoorden, maar
  als de bestanden de wereld ingaan zou een review van PR04 zeker helpen.
  Verder helpt het previewportaal ook. Echter dat is niet beschikbaar Lennert
  gaat daar aan trekken

- Naast voorbeeldbestanden is een belangrijke output van de werkplaats een
  rapportage met bevindingen. Ik heb beloofd dat die er komt (en min of meer
  geimpliceerd dat ik daaraan zou trekken) maar bij nader inzien is dat niet
  heel verstandig (ik heb het nog even druk met andere dingen verder is het wat
  raar als ik als ontwikkelaar van de standaard ook verantwoordelijk wordt voor
  de bevindingen over de standaard). Mirella, zou jij hier een oplossing voor
  kunnen bedenken?

- We hebben het ook nog gehad over een vervolg in de volgende PI. Dat werd zeer
  nuttig bevonden. Echter: het BG is nogal teleurgesteld dat de standaard nog
  lang niet zo stabiel is als beloofd was en gaat zeker wachten met nog iets
  doen tot het stof rondom de taksforce is neergedaald. Ik begreep net al van
  René dat er al beweging is op het gebied van een vervolg dus dat is al belegd
  begrijp ik.

Het idee voor huiskwerk voor de volgende keer is om je besluit door het previewportaal te krijgen. Echter omdat dit nog niet opengesteld is kan dit nog niet.





