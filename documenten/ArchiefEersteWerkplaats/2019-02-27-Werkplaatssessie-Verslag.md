# Verslag werkplaatssessie 2019-02-27

Opgesteld door Mirella vd Velde. Omgezet naar markdown door @wilkoquak

## Modellering van werkingsgebieden

Onderscheid tussen locaties en werkingsgbieden: locaties zijn onderdelen van
geodatacollecties, dus gml-coördinaten, en de werkingsgebieden zijn ook gml,
maar die hangen specifiek aan artikelen en leden.

Een locatie kan hangen aan een activiteit, of functietoedeling, of beperkingengebied.

Zijn beide informatieobjecten.

## Wat zijn kenmerken?

Dit zijn eigenschappen van een regeltekst. Niet juridisch. Daar kunnen ze aan worden gekoppeld: dus artikel, lid of deel daarvan

Dit zijn onderwerp, thema, regelkwalificatie, (normadressaat), activiteit, functie en beperkingengebied.

Er zit overlap in met wat in informatieobject (geodatacollectie) zit voor:

- Onderwerp

- Activiteit

- Functie en

- Beperkingengebied

Een activiteit kan én als informatieobject (dus juridisch) én als kenmerk worden uitgedrukt: wil je uitdrukken dat een locatie aan een activiteit hangt, of dat een tekst over een activiteit gaat.

 

## Wanneer informatieobjecten gebruiken en wanneer kenmerken?

Dringend advies vanuit de werkplaats:

Helderheid en primaire keuzes maken. Vanuit de werkplaats is helder dat de voorkeur heeft om te kiezen voor informatieobjecten, en niet voor kenmerken, omdat:

- een kenmerk niet terugkomt in de geo dus voor burger niet te vinden is

- Als deze mogelijkheden leiden tot verschillende keuzes tussen bevoegd gezagen, worden de werkwijzes verschillend en dat is softwarematig, en dus voor softwareleveranciers, zeer onhandig

- Het stelsel onderuit haalt omdat het niet te vinden is voor burger

 

## Modellering van kenmerken

- Kenmerken zijn de eigenschappen die je aan een regeltekst hangt.
- Kenmerkgroep is altijd verplicht, ook als er maar een is
- Je gebruikt een kenmerkgroep om onderlinge samenhang aan te geven.
- Als er bv. voorwaardelijke zaken zitten aan een kenmerk, stop je die in een kenmerkgroep.

## Waardenlijst

Er zijn limitatieve en uitbreidbare waardenlijsten. Aan die laatste, de open lijsten, wordt nu ontwikkeld, hoe dat procedureel gaat werken en richting catalogus gaat gaan.

Gesloten waardenlijsten: worden eigenlijk nooit meer gewijzigd.

 

## Uit het huiswerk van 20 feb

Je hebt een work id en een expression id, ipv unieke id. Work id niet zo waardevol

Functietoedeling: alleen hiervoor is een toelichting

Activiteitentoedeling: niet te vinden in de toelichting, beperkingentoedeling, normen en waarden ook niet.

Conform functietoedeling, maak je ook activiteitentoedeling. De toelichtingen ervoor volgen nog.

## Actie: Metadatavoorbeeldbestand wordt op gitlab gezet – actie Gerard/Sjors/Wilko

Deel van de referenties zit in de bijlage, wat raar is, bijlage is dus altijd verplicht.

Actie: Je wilt een begrip verwijzing waar ook een begrip van gemaakt moet worden -
dus koppeling met begrippenlijst. Dit moet uitgezocht worden – actie
Gerard/Sjors/Wilko.

Zou je moeten kunnen nesten. Wellicht met ankers werken? Geldt als algemene
vraag, geoobject, begrip, etc.

Elk listitem een id meegeven is echt erg ingewikkeld om te bouwen - omdat dat dan allemaal aparte tabellen/modules en is erg gebruikersonvriendelijk. Bij listitem : als er iets vervangen wordt in listitem, wordt hele listitem vervangen.

Bij tabellen geldt waarschijnlijk hetzelfde.

Afspraak: als je een lid van een artikel een werkingsgebied geeft, moet je alle
leden een werkingsgebied geven. Je kunt ook alleen een niveau hoger een
werkingsgebied toevoegen.

Op een besluit zou geen besluitgebied worden benoemd, maar dat is wel nodig. Dit
behoort vanuit het bevoegd gezag te worden aangeleverd. Initiële besluit moet
een geoverwijzing bevatten.

## Huiswerk voor volgende keer:

werkingsgebieden, kenmerken en kenmerkgroepen goed toevoegen.

## Volgende week:

- ook matrix behandelen, zie bijgevoegd
- onderwerp volgende keer is inbedding in besluit, inclusief mutatiebesluit.
- Actie: Welke mutaties kunnen er al? Lijst van Tanja. Graag zo snel mogelijk rondsturen. Is voor zowel tekst als geo.
